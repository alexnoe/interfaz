package Datos;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import Interfaz.Contacto;

public class IoDatos {
	public static boolean comprobarLogin(String usuario, String contra) {
		
		boolean contraseņa=false;
		File fichero = new File("usuarios.txt");
		if(!fichero.exists()) {
			try {
				fichero.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} 
		
		try {
			Scanner leer = new Scanner(fichero);
			while (leer.hasNext()) {
				String linea[] = leer.nextLine().split("-");
				if(linea[0].equalsIgnoreCase(usuario) && linea[1].equalsIgnoreCase(contra) ) {
					contraseņa = true;
					break;
				}
			} 
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return contraseņa;
	}
	
	public static ArrayList< Contacto> cargarContactos(String usuario) {
		ArrayList<Contacto> vContactos = new ArrayList<Contacto>();
		
		File ficheroContacto = new File(usuario+".txt");
		
		if(!ficheroContacto.exists()) {
			try {
				ficheroContacto.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		try {
			Scanner leer = new Scanner(ficheroContacto);
			
			while(leer.hasNextLine()) {
				String contacto;
				
				contacto = leer.nextLine();
				
			}
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return vContactos;
	}
	
	public static void guardarUsuario(String usuario, String contacto, int telefono) {
		
		File ficheroContacto = new File(usuario+".txt");
		
		if(!ficheroContacto.exists()) {
			try {
				ficheroContacto.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		try {
			
				FileWriter fw = new FileWriter(ficheroContacto, true);
				PrintWriter pw = new PrintWriter(fw);
				
				pw.print("Nombre: "+contacto+" TLFN: "+telefono+"\n");
				pw.close();
				fw.close();
				
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	}
}
