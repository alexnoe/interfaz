package Interfaz;


import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Datos.IoDatos;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class InterfazLogin extends JFrame {

	private JPanel contentPane;
	private JLabel lblUsuario;
	private JTextField textUsuario;
	private JLabel lblContraseña;
	private JTextField textContraseña;
	private JButton btnEntrar;
	private JButton btnRegister;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazLogin frame = new InterfazLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InterfazLogin() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 586, 447);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblUsuario = new JLabel("Usuario:");
		lblUsuario.setBounds(205, 57, 154, 22);
		contentPane.add(lblUsuario);
		
		textUsuario = new JTextField();
		textUsuario.setBounds(205, 90, 86, 20);
		contentPane.add(textUsuario);
		textUsuario.setColumns(10);
		
		lblContraseña = new JLabel("Contrase\u00F1a:");
		lblContraseña.setBounds(205, 155, 86, 14);
		contentPane.add(lblContraseña);
		
		textContraseña = new JTextField();
		textContraseña.setBounds(205, 180, 86, 20);
		contentPane.add(textContraseña);
		textContraseña.setColumns(10);
		
		btnEntrar = new JButton("Entrar");
		btnEntrar.addMouseListener(new BtnEntrarMouseListener());
		btnEntrar.setBounds(70, 247, 89, 23);
		contentPane.add(btnEntrar);
		
		btnRegister = new JButton("Registrarse");
		btnRegister.addMouseListener(new BtnRegisterMouseListener());
		btnRegister.setBounds(359, 247, 89, 23);
		contentPane.add(btnRegister);
	}
	private class BtnEntrarMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			String usuario = "";
			String contra="";
			
			usuario = textUsuario.getText();
			contra = textContraseña.getText();
			
			if(IoDatos.comprobarLogin(usuario, contra)) {
				
				InterfazDinamica id = new InterfazDinamica(usuario);
				id.setVisible(true);
				dispose();
				
			}	
			
		}
	}
	private class BtnRegisterMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			InterfazRegistro ir = new InterfazRegistro();
			
			ir.setVisible(true);
			dispose();
		}
	}
}