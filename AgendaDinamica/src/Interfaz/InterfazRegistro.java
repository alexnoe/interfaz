package Interfaz;


import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class InterfazRegistro extends JFrame {

	private JPanel contentPane;
	private JLabel lblUsuario;
	private JTextField textUsu;
	private JLabel lblContra;
	private JTextField textContra;
	private JButton btnRegistrar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazRegistro frame = new InterfazRegistro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InterfazRegistro() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 601, 474);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblUsuario = new JLabel("Usuario:");
		lblUsuario.setBounds(205, 53, 112, 21);
		contentPane.add(lblUsuario);
		
		textUsu = new JTextField();
		textUsu.setBounds(205, 85, 86, 20);
		contentPane.add(textUsu);
		textUsu.setColumns(10);
		
		lblContra = new JLabel("Contrase\u00F1a:");
		lblContra.setBounds(205, 125, 86, 14);
		contentPane.add(lblContra);
		
		textContra = new JTextField();
		textContra.setBounds(205, 150, 86, 20);
		contentPane.add(textContra);
		textContra.setColumns(10);
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.addMouseListener(new BtnRegistrarMouseListener());
		btnRegistrar.setBounds(205, 230, 89, 23);
		contentPane.add(btnRegistrar);
	}

	private class BtnRegistrarMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			
			String nombre="";
			String contraseņa="";
			
			nombre = textUsu.getText();
			contraseņa = textContra.getText();
			
			File fichero = new File("usuarios.txt");
			if(!fichero.exists()) {
				try {
					fichero.createNewFile();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else {
				try {
					FileWriter fw = new FileWriter(fichero, true);
					PrintWriter pw = new PrintWriter(fw);
					
					pw.println(nombre+"-"+contraseņa);
					pw.close();
					
					InterfazLogin il = new InterfazLogin();
					il.setVisible(true);
					dispose();
					
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
			}
		}
	}
}