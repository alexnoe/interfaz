package Interfaz;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Datos.IoDatos;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JList;

public class InterfazDinamica extends JFrame {

	private JPanel contentPane;
	private JLabel lblTitulo;
	private JLabel lblNombre;
	private JLabel lblTelefono;
	private JTextField textNombre;
	private JTextField textTelef;
	private JButton btnGuardar;
	private JLabel lblContacto;
	private DefaultComboBoxModel modeloCombobox;
	private JTextArea textAreaContactos;
	private JButton btnBorrar;
	private ArrayList<Contacto> vPersonasD;
	private JButton btnAtras;
	private String usuario;
	private JList listContactos;
	private JTextField textBuscar;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazDinamica frame = new InterfazDinamica("");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public InterfazDinamica(String usuario) {
		this.usuario = usuario;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 657, 446);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblTitulo = new JLabel("Agenda Telefono");
		lblTitulo.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setBounds(155, 23, 330, 44);
		contentPane.add(lblTitulo);

		lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(87, 132, 131, 20);
		contentPane.add(lblNombre);

		lblTelefono = new JLabel("Telefono:");
		lblTelefono.setBounds(87, 198, 131, 20);
		contentPane.add(lblTelefono);

		textNombre = new JTextField();
		textNombre.setBounds(87, 167, 86, 20);
		contentPane.add(textNombre);
		textNombre.setColumns(10);

		textTelef = new JTextField();
		textTelef.setBounds(87, 229, 86, 20);
		contentPane.add(textTelef);
		textTelef.setColumns(10);

		btnGuardar = new JButton("Guardar");
		btnGuardar.addMouseListener(new BtnGuardarMouseListener());
		btnGuardar.setBounds(87, 282, 89, 23);
		contentPane.add(btnGuardar);

		lblContacto = new JLabel("Contactos:");
		lblContacto.setBounds(463, 120, 65, 14);
		contentPane.add(lblContacto);
		modeloCombobox = new DefaultComboBoxModel();
		modeloCombobox.addElement("Contactos");

		textAreaContactos = new JTextArea();
		textAreaContactos.setBounds(87, 326, 478, 70);
		contentPane.add(textAreaContactos);

		btnBorrar = new JButton("Borrar");
		btnBorrar.addMouseListener(new BtnBorrarMouseListener());
		btnBorrar.setBounds(200, 282, 89, 23);
		contentPane.add(btnBorrar);
		
		btnAtras = new JButton("Atras");
		btnAtras.addMouseListener(new BtnAtrasMouseListener());
		btnAtras.setBounds(311, 282, 89, 23);
		contentPane.add(btnAtras);
		
		listContactos = new JList();
		listContactos.setBounds(428, 189, 131, 114);
		contentPane.add(listContactos);
		DefaultListModel<String> modelo = new DefaultListModel<String>();
		listContactos.setModel(modelo);
		
		textBuscar = new JTextField();
		textBuscar.setBounds(449, 145, 86, 20);
		contentPane.add(textBuscar);
		textBuscar.setColumns(10);
		vPersonasD = IoDatos.cargarContactos(usuario);
	//	vPersonasD.add(new Contacto("juan", 123));
		modelo.clear();
		for (Contacto c : vPersonasD) {
			modelo.addElement(c.getNombre());
		}
	}



	private class BtnGuardarMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent arg0) {

			String nombre="";
			int telefono=0;

			nombre = textNombre.getText();
			telefono = Integer.parseInt(textTelef.getText());

			Contacto aux = new Contacto(nombre, telefono);

			vPersonasD.add(aux);

			textNombre.setText("");
			textTelef.setText("");

			modeloCombobox.removeAllElements();
			modeloCombobox.addElement("Contactos: ");
			
			//InterfazLogin il = new InterfazLogin();
	
			IoDatos.guardarUsuario(usuario, nombre, telefono);
			
			String lista ="";
			

			String salida = "";

			for (int i = 0; i < vPersonasD.size(); i++) {
				salida += vPersonasD.get(i)+"\n";
			}
			
			JOptionPane.showMessageDialog(null, "Contacto guardado");
			
			textAreaContactos.setText(salida);
			
		}

		
	}
	private class BtnAtrasMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent arg0) {
			InterfazLogin il = new InterfazLogin();
			
			JOptionPane.showMessageDialog(null, "Sesion cerrada","Log Out", 3);
			il.setVisible(true);
			dispose();
		}
	}
	private class BtnBorrarMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent arg0) {
		}
	}
}